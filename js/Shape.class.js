class Shape {
    constructor(id, x, y){
        this._id = id;
        this._x = x;
        this._y = y;
    }
    move(x,y){
        this._x = x;
        this._y = y;
        return "id: "+this._id+", x: "+x+", y: "+y;
    }
}