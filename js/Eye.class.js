class Eye extends Circle{
    constructor (id, x, y, radius, color){
        super(id, x, y, radius);
        this._color = color;
    }
    roll(rollAmount){
        return this.move(this._x+rollAmount, this._y);
    }
}