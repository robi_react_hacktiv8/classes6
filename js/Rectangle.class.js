class Rectangle extends Shape {
    constructor(id, x, y, width, height){
        super(id, x, y);
        this._width = width;
        this._height = height;
    }
}