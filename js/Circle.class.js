class Circle extends Shape{
    constructor (id, x, y, radius){
        super(id, x, y);
        this._radius = radius;
    }
}